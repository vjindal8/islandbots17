package org.firstinspires.ftc.islandbots17;

import android.media.AudioManager;
import android.media.SoundPool;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;

/**
 * Created by henry on 2/24/17.
 */

@com.qualcomm.robotcore.eventloop.opmode.TeleOp(name = "Light/sound display", group = "Competition Bot")
public class PresentationMode extends LinearOpMode {
    CompetitionBot robot;

    public void runOpMode() throws InterruptedException {
        SoundPool soundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 0);
        int beep = soundPool.load(hardwareMap.appContext, R.raw.horn, 1);


        robot = new CompetitionBot(hardwareMap, telemetry, true);

        waitForStart();

        ElapsedTime timer = new ElapsedTime();

        while (opModeIsActive()) {
            if (gamepad1.a && ((int)timer.milliseconds() / 300) % 2 == 0) {
                robot.light.setPower(1);
                if ((int) timer.milliseconds() % 600 == 0) {
                    soundPool.play(beep, 1, 1, 1, 0, 1);
                }
            } else {
                robot.light.setPower(0);
            }
        }

    }
}
