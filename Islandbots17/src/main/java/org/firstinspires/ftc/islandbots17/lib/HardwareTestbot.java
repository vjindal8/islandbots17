package org.firstinspires.ftc.islandbots17.lib;

import com.qualcomm.hardware.adafruit.BNO055IMU;
import com.qualcomm.hardware.adafruit.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.AnalogSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.Acceleration;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.Velocity;

/**
 * Created by Lev on 9/19/16.
 */

public class HardwareTestbot {
    public DcMotor leftRearMotor;
    public DcMotor rightFrontMotor;
    public DcMotor leftFrontMotor;
    public DcMotor rightRearMotor;
    private BNO055IMU imu;
    public OpticalDistanceSensor line0;


    /* local OpMode members. */

    /* Initialize standard Hardware interfaces */
    public HardwareTestbot(HardwareMap hwMap) {
        // Define and Initialize Motors
        leftRearMotor = hwMap.dcMotor.get("lrMotor");
        rightFrontMotor = hwMap.dcMotor.get("rfMotor");
        leftFrontMotor = hwMap.dcMotor.get("lfMotor");
        rightRearMotor = hwMap.dcMotor.get("rrMotor");

        leftFrontMotor.setDirection(DcMotor.Direction.REVERSE);

        leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftRearMotor.setMode((DcMotor.RunMode.STOP_AND_RESET_ENCODER));
        rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightRearMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftRearMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightRearMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit           = BNO055IMU.AngleUnit.DEGREES;
        parameters.calibrationDataFile = "AdafruitIMUCalibration.json"; // see the calibration sample opmode
        imu = hwMap.get(BNO055IMU.class, "imu");
        imu.initialize(parameters);

        line0 = hwMap.opticalDistanceSensor.get("line0");


        leftFrontMotor.setPower(0);
        leftRearMotor.setPower(0);
        rightRearMotor.setPower(0);
        rightFrontMotor.setPower(0);
    }

    public double getZAngle() {
        return -imu.getAngularOrientation().toAxesReference(AxesReference.INTRINSIC).toAxesOrder(AxesOrder.ZYX).firstAngle;
    }
}
