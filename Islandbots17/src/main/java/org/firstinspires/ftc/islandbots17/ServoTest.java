package org.firstinspires.ftc.islandbots17;

import android.media.AudioManager;
import android.media.SoundPool;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;
import org.firstinspires.ftc.islandbots17.lib.TeleopButton;

import static java.lang.Math.abs;

/**
 * Created by Lev on 9/19/16.
 */

@TeleOp(name = "Servo test", group = "Testbot")
public class ServoTest extends LinearOpMode {
    @Override
    public void runOpMode() throws InterruptedException {
        Servo loader = hardwareMap.servo.get("loader");

        waitForStart();

        while (opModeIsActive()) {
            if (gamepad1.dpad_up) {
                loader.setPosition(loader.getPosition() + 0.01);
            } else if (gamepad1.dpad_down) {
                loader.setPosition(loader.getPosition() - 0.01);
            }
            telemetry.addData("pos", loader.getPosition());
            telemetry.update();
            sleep(50);
        }
    }
}
