package org.firstinspires.ftc.islandbots17.lib;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by Lev on 10/3/16.
 */

public abstract class Autonomous extends LinearOpMode {
    protected CompetitionBot robot;

    private double clamp(double power) {
        if (power > 1) {
            return 1;
        }
        if (power < -1) {
            return -1;
        }
        return power;
    }

    private double ramp(double position, double distance, double speed) {
        double min_speed = 0.2;
        double ramp_dist = 300; //Distance over which to do ramping
        //make sure all inputs are positive
        position = Math.abs(position);
        distance = Math.abs(distance);

        speed = Math.abs(speed);
        double speed_delta = speed - min_speed;
        if (speed_delta <= 0) { //requested speed is below minimal
            return min_speed;
        }
        //adjust ramping distance for short distances
        if (distance < 3 * ramp_dist) {
            ramp_dist = distance / 3;
        }
        //now compute the desired speed
        if (position < ramp_dist) { //ramping up
            return min_speed + speed_delta * (position / ramp_dist);
        }
        if (position > distance) { //overshoot
            return 0; //driection of motion will be taken care of in forward function
        }
        if (position > distance - ramp_dist * 2) { //ramping down
            return min_speed + speed_delta * (distance - position) / (ramp_dist * 2);
        }

        return speed; //default
    }

    public void setMotors(double left, double right) throws InterruptedException {

        robot.leftFrontMotor.setPower(left);
        robot.leftRearMotor.setPower(left);
        robot.rightFrontMotor.setPower(right);
        robot.rightRearMotor.setPower(right);

    }

    public void forward(double speed, double distance) throws InterruptedException {
        if (distance < 0) {

            return;
        }
        robot.resetDriveEncoders();

        double targetPos = robot.leftFrontMotor.getCurrentPosition() + distance;

        while (targetPos > robot.leftFrontMotor.getCurrentPosition() && opModeIsActive()) {
            double correction = 0.01*(robot.rightFrontMotor.getCurrentPosition()-robot.leftFrontMotor.getCurrentPosition());


            double position = robot.leftFrontMotor.getCurrentPosition();

            double ramped_speed = ramp(position, targetPos, speed);
            setMotors(clamp(ramped_speed + correction),
                    clamp(ramped_speed - correction));

        }
        setMotors(0, 0);
    }

    public void backward(double speed, double distance) throws InterruptedException {
        if (distance < 0) {

            return;
        }
        robot.resetDriveEncoders();

        double targetPos = robot.leftFrontMotor.getCurrentPosition() - distance;

        while (robot.leftFrontMotor.getCurrentPosition() > targetPos && opModeIsActive()) {
            double correction = 0.01*(robot.rightFrontMotor.getCurrentPosition()-robot.leftFrontMotor.getCurrentPosition());


            double position = robot.leftFrontMotor.getCurrentPosition();

            double ramped_speed = ramp(position, targetPos, speed);
            setMotors(clamp(-ramped_speed + correction),
                    clamp(-ramped_speed - correction));

        }
        setMotors(0, 0);
    }

    public void turn(double speed, double angleDelta) throws InterruptedException {


        double currentAngle = robot.getZAngle();
        double targetAngle = (currentAngle + angleDelta) % 360;
        double diff = angleDiff(currentAngle, targetAngle);
        double direction = diff > 0 ? 1 : -1;

        while (diff * direction > 0 && opModeIsActive()) {
            if (Math.abs(diff) < 10) {
                speed = 0.1;
            }
            setMotors(speed * direction, -speed * direction);

            currentAngle = robot.getZAngle();
            diff = angleDiff(currentAngle, targetAngle);

        }
        setMotors(0, 0);
    }

    private double angleDiff(double angle1, double angle2) {
        double d1 = angle2 - angle1;
        if (d1 > 180) {
            return d1 - 360;
        } else if (d1 < -180) {
            return d1 + 360;
        } else {
            return d1;
        }
    }

    public void performAutonomous(boolean isRedAutonomous) throws InterruptedException {
        int turnDirection = isRedAutonomous ? 1 : -1;

        robot = new CompetitionBot(hardwareMap, telemetry, true);

        robot.color.enableLed(false);

        waitForStart();


        //ramp up the flywheel shooter
        class RampFlyWheel extends Thread {
            DcMotor shooter;

            RampFlyWheel( DcMotor shooter) {
                this.shooter = shooter;

            }

            public void run() {
                for (int i = 1; i <=10; i++) {
                    robot.shooter.setPower(robot.SHOOTER_SPEED_AUTONOMOUS *i / 10);
                    try {
                        sleep(50);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        }

        RampFlyWheel rampFlyWheel = new RampFlyWheel(robot.shooter);
        rampFlyWheel.run();

        forward(0.7, 1100);

        sleep(1500);


        shootBalls();

        // SHOOTER CODE COMPLETE

        turn(0.3, isRedAutonomous ? 110 : -100);

        // Compensate for shooter asymmetry
        // Will probably need adjustment
        backward(0.7, isRedAutonomous ? 1950 : 2700);

        turn(0.3, isRedAutonomous ? 40 : -45);

        approachBeaconLineAndPressButton(isRedAutonomous);
        forward(0.7, 700);
        turn(0.3, turnDirection * 80);


        backward(0.5, 2000);
        approachBeaconLineAndPressButton(isRedAutonomous);

//        go to base
        forward(1, 600);
        turn(0.8, 30 * turnDirection);
        forward(1, 3000);
    }

    public void shootBalls() {
        robot.loader.setPosition(robot.LOADER_DOWN);
        sleep(200);
        robot.loader.setPosition(robot.LOADER_UP);
        sleep(500);
        robot.loader.setPosition(robot.LOADER_DOWN);
        sleep(200);
        robot.loader.setPosition(robot.LOADER_UP);
        sleep(500);
        robot.loader.setPosition(robot.LOADER_DOWN);
        sleep(200);
        robot.loader.setPosition(robot.LOADER_UP);

        robot.shooter.setPower(0);

    }

    private void approachBeaconLineAndPressButton(boolean isRedAutonomous) throws InterruptedException {
        int turnDirection = isRedAutonomous ? 1 : -1;

        // go until we see the white line
        double approachSpeed = -0.25;
        double light;

        if (isRedAutonomous)
            light = robot.outsideLineR.getLightDetected();
        else
            light = robot.outsideLineL.getLightDetected();

        while (light < 0.3 && opModeIsActive()) {

            setMotors(approachSpeed, approachSpeed);
            if (isRedAutonomous)
                light = robot.outsideLineR.getLightDetected();
            else
                light = robot.outsideLineL.getLightDetected();
        }
        setMotors(0, 0);

        backward(0.4, 180);

        double speed = 0.3;

        // Prevent robot from following red line
        turn(speed, -45 * turnDirection);

        while (((isRedAutonomous ? robot.lineR.getLightDetected() : robot.lineL.getLightDetected()) < 0.3) && opModeIsActive()) {

            setMotors(turnDirection * -speed, turnDirection * speed);

        }
        setMotors(0, 0);

        //now, for line following, use RUN_WITHOUT_ENCODER mode
        robot.runWithoutEncoders();

        ElapsedTime beaconTimer = new ElapsedTime();

        //now, follow the line
        double lineFollowSpeed = -0.15;

        //0.30 was orginal for wall distance
        double distance = robot.wallDistance.getLightDetected();
        while (distance < 0.20 && opModeIsActive()) {
            if (beaconTimer.time() > 5) {
                setMotors(0, 0);
                return;
            }
            // Vary line correction based on distance from beacon
            double lineFollowCorrection = Math.max(0.6 - 2 * distance, 0.25);
            double correction = lineFollowCorrection * (robot.lineL.getLightDetected() - robot.lineR.getLightDetected());
            distance = robot.wallDistance.getLightDetected();
            telemetry.addData("distnace", distance);
            telemetry.update();
            setMotors(lineFollowSpeed + correction, lineFollowSpeed - correction);
        }

        setMotors(0, 0);


        //push the beacons
        boolean pressLeft = isRedAutonomous ? robot.color.red() > robot.color.blue() : robot.color.red() < robot.color.blue();


        class Retract extends Thread {
            CRServo beacon;
            int delay;
            int power;

            Retract(CRServo beacon, int power, int delay) {
                this.beacon = beacon;
                this.power = power;
                this.delay = delay;
            }

            public void run() {
                beacon.setPower(power);
                try {
                    sleep(delay);
                } catch (InterruptedException e) {
                    return;
                }
                beacon.setPower(0);
            }
        }

        // Beacon pressers were plugged in opposite ports. Right is left and left is right
        // (All directions in terms of proper front of the robot)


        Retract r;
        if (pressLeft) {
            robot.beaconR.setPower(1);
            sleep(1500);
            robot.beaconR.setPower(0);
            r = new Retract(robot.beaconR, -1, 1500);

        } else {
            robot.beaconL.setPower(-1);
            sleep(1500);
            robot.beaconL.setPower(0);
            r = new Retract(robot.beaconL, 1, 1500);
        }

        backward(0.3, 50);
        r.start();

        setMotors(0, 0);

    }

}
