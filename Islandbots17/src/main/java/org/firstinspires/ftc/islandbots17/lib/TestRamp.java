package org.firstinspires.ftc.islandbots17.lib;
/*
 Gradually ramps up speed, starting from hard-coded minimal speed up to given speed. Uses target distance and current position 
 Returns a positive number, between min_speed and abs(speed)
*/
public class TestRamp {
    private double ramp(double position, double distance, double speed) {
        double min_speed = 0.15;
        double ramp_dist = 200; //FIXME!!!! distance over which to do ramping
        //make sure all inputs are positive
        position = Math.abs(position);
        distance = Math.abs(distance);
        speed = Math.abs(speed);
        double speed_delta = speed - min_speed;
        if (speed_delta <= 0) { //requested speed is below minimal
            return min_speed;
        }
        //adjust ramping distance for short distances
        if (distance < 2 * ramp_dist) {
            ramp_dist = 0.5 * distance;
        }
        //now compute the desired speed
        if (position < ramp_dist) { //ramping up
            return min_speed + speed_delta * (position / ramp_dist);
        }
        if (position > distance) { //overshoot
            return 0;
        }
        if (position > distance - ramp_dist) { //ramping down
            return min_speed + speed_delta * (distance - position) / ramp_dist;
        }

        return speed; //default
    }
}