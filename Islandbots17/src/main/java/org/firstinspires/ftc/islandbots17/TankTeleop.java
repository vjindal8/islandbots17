package org.firstinspires.ftc.islandbots17;

import android.media.AudioManager;
import android.media.SoundPool;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;
import org.firstinspires.ftc.islandbots17.lib.TeleopButton;

import static java.lang.Math.abs;

/**
 * Created by Lev on 9/19/16.
 */

@TeleOp(name = "Teleop", group = "Testbot")
public class TankTeleop extends LinearOpMode {
    CompetitionBot robot;
    private ElapsedTime speedTimer = new ElapsedTime();

    @Override
    public void runOpMode() throws InterruptedException {
        TeleopButton shooter = new TeleopButton(500, true);
        TeleopButton sweeper = new TeleopButton(500, true);
        TeleopButton loader = new TeleopButton(400, false); // real delay is half of specified delay (200ms up, 200ms down)
        TeleopButton shooterSpeedUp = new TeleopButton(200, false);
        TeleopButton shooterSpeedDown = new TeleopButton(200, false);

        SoundPool soundPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 0);
        int beep = soundPool.load(hardwareMap.appContext, R.raw.horn, 1);


        robot = new CompetitionBot(hardwareMap, telemetry, false);

        /* Initialize the hardware variables.
         * The init() method of the hardware class does all the work here
         */

        // Send telemetry message to signify robot waiting;
        //telemetry.addData("Say", "Hello Driver");
        //telemetry.update();

        // Wait for the game to start (driver presses PLAY)
        waitForStart();

        double previousTime = speedTimer.milliseconds();


        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            // Read gamepad input
            double leftJoystick = -gamepad1.left_stick_y;
            double rightJoystick = -gamepad1.right_stick_y;
            boolean shootButton = gamepad1.b;
            boolean reverseShooterButton = gamepad1.x;
            boolean sweeperButton = gamepad1.right_bumper;
            boolean loaderButton = gamepad1.a;
            boolean sweeperReverseButton = gamepad1.left_bumper;
            boolean slowMovementButton = gamepad1.right_trigger > 0.5;
            boolean shooterSpeedUpButton = gamepad1.dpad_up;
            boolean shooterSpeedDownButton = gamepad1.dpad_down;


            if (speedTimer.milliseconds()  - previousTime > 1000) {
                previousTime = speedTimer.milliseconds();
            }

            //telemetry.addData("rShoot", (robot.shooter.getCurrentPosition() - rShooterPosition) / (speedTimer.milliseconds() - previousTime));
            //telemetry.addData("lShoot", (robot.lShooter.getCurrentPosition() - lShooterPosition) / (speedTimer.milliseconds() - previousTime));
            //telemetry.update();

            // tank drive
            {
                double speed = slowMovementButton ? 0.5 : 1;
                double leftSpeed = leftJoystick * leftJoystick * speed * (leftJoystick > 0 ? 1 : -1);
                double rightSpeed = rightJoystick * rightJoystick * speed * (rightJoystick > 0 ? 1 : -1);
                robot.leftRearMotor.setPower(leftSpeed);
                robot.leftFrontMotor.setPower(leftSpeed);
                robot.rightRearMotor.setPower(rightSpeed);
                robot.rightFrontMotor.setPower(rightSpeed);
            }

            shooterSpeedUp.update(shooterSpeedUpButton);
            if (shooterSpeedUp.justPressed) {
                robot.shooterSpeedTeleop += 0.01;
                if (robot.shooterSpeedTeleop > 1) {
                    robot.shooterSpeedTeleop = 1;
                }
            }

            shooterSpeedDown.update(shooterSpeedDownButton);
            if (shooterSpeedDown.justPressed) {
                robot.shooterSpeedTeleop -= 0.01;
                if (robot.shooterSpeedTeleop < 0) {
                    robot.shooterSpeedTeleop = 0;
                }
            }

            telemetry.addData("shooter speed", robot.shooterSpeedTeleop);
            telemetry.update();

            // shooter
            shooter.update(shootButton);
            if (shooter.on) {
                double speed = Math.min(shooter.buttonTimer.milliseconds() - shooter.timePressed, 1000) / 1000.0 * robot.shooterSpeedTeleop;
                double direction = reverseShooterButton ? -1 : 1;
                //telemetry.addData("speed", speed);
                robot.shooter.setPower(direction * speed);
            } else {
                robot.shooter.setPower(0);
            }

            // sweeper
            sweeper.update(sweeperButton);
            if (sweeper.on) {
                if (sweeperReverseButton) {
                    robot.sweeper.setPower(-1);
                } else {
                    robot.sweeper.setPower(1);
                }
            } else {
                robot.sweeper.setPower(0);
            }

            // loader
            loader.update(loaderButton);
            if (loader.on && shooter.on && loader.buttonTimer.milliseconds() - loader.timePressed < loader.delay / 2) {
                robot.loader.setPosition(robot.LOADER_DOWN);
                robot.light.setPower(1);
                if (loader.justPressed) {
                    soundPool.play(beep, 1, 1, 1, 0, 1);
                }
            } else {
                robot.loader.setPosition(robot.LOADER_UP);
                robot.light.setPower(0);
            }

//            telemetry.addData("Motor Left Front", robot.leftFrontMotor.getCurrentPosition());
//            telemetry.addData("Motor Left Rear", robot.leftRearMotor.getCurrentPosition());
//            telemetry.addData("Motor Right Front", robot.rightFrontMotor.getCurrentPosition());
//            telemetry.addData("Motor Right Rear", robot.rightRearMotor.getCurrentPosition());
//            telemetry.addData("Orientation", robot.getZAngle());
            //telemetry.update();

            idle();
        }
    }
}
