package org.firstinspires.ftc.islandbots17.lib;

import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by henry on 11/7/16.
 */

public class TeleopButton {
    public ElapsedTime buttonTimer = new ElapsedTime();

    public boolean on = false;
    public boolean justPressed = false;
    public double timePressed = 0;
    public double delay;
    private boolean isToggle;

    public TeleopButton(double delay, boolean isToggle) {
        this.delay = delay;
        this.isToggle = isToggle;
    }

    public void update(boolean buttonPressed) {
        if (justPressed) {
            justPressed = false;
        }
        if (buttonTimer.milliseconds() - timePressed > delay) {
            if (isToggle) {
                if (buttonPressed) {
                    timePressed = buttonTimer.milliseconds();
                    on = !on;
                }
            } else {
                on = buttonPressed;
                if (buttonPressed) {
                    timePressed = buttonTimer.milliseconds();
                    justPressed = true;
                }
            }
        }
    }
}
