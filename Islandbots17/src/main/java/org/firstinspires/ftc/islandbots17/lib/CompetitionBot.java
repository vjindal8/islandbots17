package org.firstinspires.ftc.islandbots17.lib;

import com.qualcomm.robotcore.hardware.AnalogInput;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.OpticalDistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.islandbots17.lib.Olly.Bno055;
import org.firstinspires.ftc.robotcore.external.Telemetry;

public class CompetitionBot {
    public static final double LOADER_UP = 0.45;
    public static final double LOADER_DOWN = 0.05;
    public static final double SHOOTER_SPEED_AUTONOMOUS = 0.58; //for autonomous only
    public double shooterSpeedTeleop = 0.53;
    public DcMotor leftRearMotor;
    public DcMotor rightFrontMotor;
    public DcMotor leftFrontMotor;
    public DcMotor rightRearMotor;
    public DcMotor sweeper;
    public DcMotor shooter;
    public DcMotor light;
    public Servo loader;
    public CRServo beaconL;
    public CRServo beaconR;
    public OpticalDistanceSensor lineL;
    public OpticalDistanceSensor lineR;
    public OpticalDistanceSensor wallDistance;
    public OpticalDistanceSensor outsideLineL;
    public OpticalDistanceSensor outsideLineR;
    public AnalogInput new_gyro;
    public ColorSensor color;
    private Bno055 imu = null;


    /* local OpMode members. */

    /* Initialize standard Hardware interfaces */
    public CompetitionBot(HardwareMap hwMap, Telemetry telemetry, boolean initializeGyro) {
        // Define and Initialize Motors
        leftRearMotor = hwMap.dcMotor.get("lrMotor");
        rightFrontMotor = hwMap.dcMotor.get("rfMotor");
        leftFrontMotor = hwMap.dcMotor.get("lfMotor");
        rightRearMotor = hwMap.dcMotor.get("rrMotor");
        sweeper = hwMap.dcMotor.get("sweeper");
        shooter = hwMap.dcMotor.get("shooter");
        light = hwMap.dcMotor.get("light");
        light.setPower(0);

        loader = hwMap.servo.get("loader");
        beaconL = hwMap.crservo.get("beaconL");
        beaconR = hwMap.crservo.get("beaconR");
        beaconR.setPower(0);
        beaconL.setPower(0);

        rightRearMotor.setDirection(DcMotor.Direction.REVERSE);
        rightFrontMotor.setDirection(DcMotor.Direction.REVERSE);

        resetDriveEncoders();

        shooter.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        shooter.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        shooter.setDirection(DcMotor.Direction.REVERSE);


        lineL = hwMap.opticalDistanceSensor.get("lineL");
        lineR = hwMap.opticalDistanceSensor.get("lineR");
        outsideLineL = hwMap.opticalDistanceSensor.get("outsideLineL");
        outsideLineR = hwMap.opticalDistanceSensor.get("outsideLineR");

        wallDistance = hwMap.opticalDistanceSensor.get("wallDistance");
        color = hwMap.colorSensor.get("color");

        new_gyro = hwMap.analogInput.get("new_gyro");

        leftFrontMotor.setPower(0);
        leftRearMotor.setPower(0);
        rightRearMotor.setPower(0);
        rightFrontMotor.setPower(0);

        loader.setPosition(LOADER_UP);

        if (initializeGyro) {
            imu = new Bno055(hwMap, "imu");
            imu.init();

            while (imu.isInitActive()) {
                imu.init_loop();
                String status = imu.isInitDone() ? "OK" : "Failed";
                telemetry.addData("Init", status);
                telemetry.update();
            }

            imu.startSchedule(Bno055.BnoPolling.FUSION, 10);      // 100 Hz
            imu.startSchedule(Bno055.BnoPolling.CALIB, 250);      // 4 Hz

            class UpdateGyro extends Thread {
                Bno055 imu;

                UpdateGyro(Bno055 imu) {
                    this.imu = imu;
                }

                public void run() {
                    while (true) {
                        imu.loop();
                        try {
                            sleep(10);
                        } catch (InterruptedException e) {
                             return;
                        }
                    }
                }
            }

            UpdateGyro updateGyro = new UpdateGyro(imu);
            updateGyro.start();
        }

    }

    public void resetDriveEncoders() {
        leftFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        leftRearMotor.setMode((DcMotor.RunMode.STOP_AND_RESET_ENCODER));
        rightFrontMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        rightRearMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        leftFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        leftRearMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        rightRearMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void runWithoutEncoders() {
        leftFrontMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightFrontMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        leftRearMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        rightRearMotor.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

    }

    public double getZAngle() {
        if (imu != null) {
            return imu.eulerX() / 16;
        } else {
            return 0;
        }
    }
}
