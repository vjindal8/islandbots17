package org.firstinspires.ftc.islandbots17;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.islandbots17.lib.Autonomous;
import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by henry on 1/5/17.
 */

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "movement test autonomous", group = "Competition Bot")
public class MovementTestAutonomous extends Autonomous {

    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry, true);
        waitForStart();
        turn(0.3, 90);
        sleep(2000);
        turn(0.3,-90);
        sleep(2000);
        forward(0.5,3000);
        sleep(200);
        backward(0.5,3000);

        /*ElapsedTime beaconTimer = new ElapsedTime();

        robot.runWithoutEncoders();
        //now, follow the line
        double lineFollowSpeed = -0.15;
        while (beaconTimer.time() <8 && opModeIsActive()) {

            // Vary line correction based on distance from beacon
            double lineFollowCorrection = Math.max(0.4 - robot.wallDistance.getLightDetected(), 0.25);
            //double lineFollowCorrection=0.15;
            double correction = lineFollowCorrection  * (robot.lineL.getLightDetected() - robot.lineR.getLightDetected());
            telemetry.addData("correction", correction);
            telemetry.update();
            setMotors(lineFollowSpeed + correction, lineFollowSpeed - correction);
        }
        */
    }
}