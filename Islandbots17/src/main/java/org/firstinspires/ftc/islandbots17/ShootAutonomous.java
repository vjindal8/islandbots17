package org.firstinspires.ftc.islandbots17;

import org.firstinspires.ftc.islandbots17.lib.Autonomous;
import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;

/**
 * Created by henry on 12/11/16.
 */


@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "shoot", group = "Competition Bot")
public class ShootAutonomous extends Autonomous {
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry, false);
        robot.color.enableLed(false);
        waitForStart();

        sleep(15 * 1000);

        //ramp up the flywheel shooter
        for (int i = 10; i >= 1; i--) {
            robot.shooter.setPower(robot.SHOOTER_SPEED_AUTONOMOUS / i);
            sleep(100);
        }

        forward(0.4, 2000);

        //shoot the balls
        sleep(1000);

        shootBalls();

        backward(0.4, 1000);

        sleep(500);

        forward(0.8, 2500);


    }

}
