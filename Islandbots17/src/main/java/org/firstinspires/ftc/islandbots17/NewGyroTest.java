package org.firstinspires.ftc.islandbots17;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.robot.Robot;

import org.firstinspires.ftc.islandbots17.lib.Autonomous;
import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;

/**
 * Created by henry on 2/16/17.
 */

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "new gyro test", group = "Competition Bot")
public class NewGyroTest extends LinearOpMode {


    private final int voltage_to_anglular_velocity = 550;
    private final double target_dt = 1.0/100;
    private double angle = 0;

    public void runOpMode() throws InterruptedException {
        CompetitionBot robot = new CompetitionBot(hardwareMap, telemetry, true);
        waitForStart();

        double calibration_time = 5.0;

        int calibration_iterations = (int)(calibration_time/target_dt);

        double calibration_sum = 0;

        for (int i = 0; i < calibration_iterations && opModeIsActive(); i++) {
            calibration_sum += robot.new_gyro.getVoltage();
            sleep((long)(target_dt*1000));
        }

        double static_voltage = calibration_sum / calibration_iterations;





        double prevTime = System.nanoTime();

        while(opModeIsActive()) {
            double voltage = robot.new_gyro.getVoltage();
            double time = System.nanoTime();

            double measured_dt = (time - prevTime) / 1_000_000_000;

            prevTime = time;

            angle += measured_dt * voltage_to_anglular_velocity * (voltage - static_voltage);

            sleep((long)(target_dt*1000));

            telemetry.addData("angle", angle);
            telemetry.addData("voltage", voltage);
            telemetry.addData("measured_dt", measured_dt);
            telemetry.update();


        }

    }
}