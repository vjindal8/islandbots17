package org.firstinspires.ftc.islandbots17;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.islandbots17.lib.CompetitionBot;
import org.firstinspires.ftc.islandbots17.lib.Olly.Bno055;

/**
 * Created by Henry on 1/2/2017.
 */
@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Sensor test", group = "Competition Bot")
public class SensorTestAutonomous extends LinearOpMode {
    private CompetitionBot robot;
    @Override
    public void runOpMode() throws InterruptedException {
        robot = new CompetitionBot(hardwareMap, telemetry, true);

        waitForStart();

        while (opModeIsActive()) {
            telemetry.addData("lineL", robot.lineL.getLightDetected());
            telemetry.addData("lineR", robot.lineR.getLightDetected());
            telemetry.addData("outsideLineL", robot.outsideLineL.getLightDetected());
            telemetry.addData("outsideLineR", robot.outsideLineR.getLightDetected());
            telemetry.addData("wallDistance", robot.wallDistance.getLightDetected());
            telemetry.addData("orientation", robot.getZAngle());
            telemetry.addData("color", robot.color.red() - robot.color.blue());
            telemetry.addData("rightFrontMotor", robot.rightFrontMotor.getCurrentPosition());
            telemetry.addData("leftFrontMotor", robot.leftFrontMotor.getCurrentPosition());
            telemetry.addData("rightRearMotor", robot.rightRearMotor.getCurrentPosition());
            telemetry.addData("leftRearMotor", robot.leftRearMotor.getCurrentPosition());
            telemetry.update();
        }
    }
}
