package org.firstinspires.ftc.islandbots17;

import org.firstinspires.ftc.islandbots17.lib.Autonomous;

/**
 * Created by Lev on 10/3/16.
 */

@com.qualcomm.robotcore.eventloop.opmode.Autonomous(name = "Red Autonomous", group = "Competition Bot")

public class RedAutonomous extends Autonomous {

    public void runOpMode() throws InterruptedException {
        performAutonomous(true);
    }
}